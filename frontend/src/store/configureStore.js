import { createStore, combineReducers, compose } from 'redux';
import { ConnectedRouter as Router, routerReducer, routerMiddleware } from "react-router-redux";

const reducers = combineReducers({ 
    router: routerReducer,
  
  });

export default reducers;