
import React, {Component} from "react";
import {Redirect, Link, Route, Switch} from "react-router-dom";
import Header from './Header';
import axios from 'axios';
import {connect} from "react-redux";
import { push } from "react-router-redux";


class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms : [],
        }
    }


    componentWillMount() {
     
    }
    

    render() {
      let image = "assets/bower_components/admin-lte/dist/img/user4-128x128.jpg";

        return (
            <div>
            <aside className="main-sidebar" style={{height: "2000px"}}>
             <section className="sidebar">
               <div className="user-panel">
                <div className="pull-left image">
                  <img src={image} className="img-circle" alt="User Image"/>
                </div>
                <div className="pull-left info">
                  <p>Marie</p>
                  <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
                </div>
              </div>
               <form action="#" method="get" className="sidebar-form">
                <div className="input-group">
                  <input type="text" name="q" className="form-control" placeholder="Search..."/>
                  <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                      </span>
                </div>
              </form>
               <ul className="sidebar-menu" data-widget="tree">
                <li className="header">NAVIGATION PRINCIPALE</li>
                <li className="active treeview">
                <Link to="/dashbord" onClick={this.props.navigateTo.bind(this, '/')}>
                 <i className="fa fa-dashboard"></i> <span>Tableau de bord</span>
                  </Link>
                </li>
                <li>
                <Link to="/communities" onClick={this.props.navigateTo.bind(this, '/users')}>
                    <i className="fa fa-th"></i>
                    <span>Communite</span>
                    <span className="pull-right-container">
                      <span className="label label-primary pull-right">4</span>
                    </span>
                  </Link>
                </li>
                <li>
                <Link to="/events" onClick={this.props.navigateTo.bind(this, '/events')}>
                    <i className="fa fa-th"></i> <span>Evenements</span>
                    <span className="pull-right-container">
                      <small className="label pull-right bg-green">new</small>
                    </span>
                  </Link>
                </li>
            </ul>
            </section>
           </aside>
           </div>
        );
    }
}

const state = (state, ownProps = {}) => {
  return {
    location: state.location,
    info: state.info
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  navigateTo: (location) => {
    dispatch(push(location));
  },

}
};

export default connect(state, mapDispatchToProps)(Sidebar);