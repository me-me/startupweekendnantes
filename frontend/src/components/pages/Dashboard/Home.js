
import React from "react";
import Header from '../../common/Header';
import Sidebar from '../../common/SideBar';
import Footer from '../../common/Footer';
import Dashboard from './Dashboard';


class Home extends React.Component {
    constructor(props) {
        super(props);
    }



    render() {
        
        return (
            <div>
                <Header />
                <Dashboard/>
                <Sidebar/>
                <Footer />
            </div>
        );
    }
}

export default Home;