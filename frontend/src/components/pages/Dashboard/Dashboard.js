
import React from "react";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
         let style = {
            width: "100%",
            height: "125px",
            fontSize: "14px",
            lineHeight: "18px", 
            border: "1px solid #dddddd", 
            padding: "10px",
        };

        let style1 = {
            marginRight: "5px"
        };

        let style2 = {
            height: "250px",
            width: "100%",
        };

        let style3 = {
            borderRight: "1px solid #f4f4f4"
        };

        let style4 = {
            height: "250px",
        };

        let style5 = {
            fontSize: "20px"
        };

        let style6 = {
            position: "relative", 
            height: "300px",
        };

        let style7 = {
            width: "100%"
        }

        
        return (
            <div>
                             <div className="content-wrapper">
     <section className="content-header">
      <h1>
          Tableau de bord
        <small>Control panel</small>
      </h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
        <li className="active">Dashboard</li>
      </ol>
    </section>

    <section className="content">
      <div className="row">
      <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-yellow">
            <div className="inner">
              <h3>6</h3>

              <p>Communauté</p>
            </div>
            <div className="icon">
              <i className="ion ion-person-add"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-green">
            <div className="inner">
              <h3>353<sup style={style5}></sup></h3>

              <p>Membres</p>
            </div>
            <div className="icon">
              <i className="ion ion-stats-bars"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <div className="col-lg-3 col-xs-6">
          <div className="small-box bg-aqua">
            <div className="inner">
              <h3>150</h3>

              <p>événement active</p>
            </div>
            <div className="icon">
              <i className="ion ion-bag"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
         <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-red">
            <div className="inner">
              <h3>6</h3>

              <p>Inter-communauté</p>
            </div>
            <div className="icon">
              <i className="ion ion-pie-graph"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       </div>

         <div class="content">

          <section class="content">

<div class="row">
  <div class="col-md-3">

    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="assets/bower_components/admin-lte/dist/img/user4-128x128.jpg" alt="User profile picture"/>

        <h3 class="profile-username text-center">Marie Laure</h3>

        <p class="text-muted text-center">Chef de projet</p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Followers</b> <a class="pull-right">1,322</a>
          </li>
          <li class="list-group-item">
            <b>Following</b> <a class="pull-right">543</a>
          </li>

        </ul>

        <a href="#" class="btn btn-primary btn-block"><b>voir plus</b></a>
      </div>
    </div>
 
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">À propos de moi</h3>
      </div>
      <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

        <p class="text-muted">
          B.S. in Computer Science from the University of Tennessee at Knoxville
        </p>

        <hr></hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

        <p class="text-muted">Malibu, California</p>

        <hr></hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Competences</strong>

        <p>
          <span class="label label-danger">UI Design</span>
          <span class="label label-success">Coding</span>
          <span class="label label-info">Javascript</span>
          <span class="label label-warning">PHP</span>
          <span class="label label-primary">Node.js</span>
        </p>

        <hr></hr>

        <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

        <p>Attentive et toujours à votre ecoute....la bienveillance et l'optimisme sont mes leitmotiv principaux.</p>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#activity" data-toggle="tab">activité recentes</a></li>
        <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
        <li><a href="#settings" data-toggle="tab">recherche</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="activity">
          <div class="post">
            <div class="user-block">
              <img class="img-circle img-bordered-sm" src="assets/bower_components/admin-lte/dist/img/user1-128x128.jpg" alt="user image"/>
                  <span class="username">
                    <a href="#">Jonathan Burke Jr.</a> a crée l’événement <b>SOCCER</b>                      <small className="label pull-right bg-green">Famille</small>
                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                  </span>
              <span class="description">partager a - 7:30 PM aujourd'hui</span>
            </div>
            <p>
            Pensez à ramener votre bonne humeur !!!! Je compte sur vous !!! A samedi
            </p>
            <div class="row margin-bottom">
              <div class="col-sm-7">
                <img class="img-responsive" src="https://s1.reutersmedia.net/resources/r/?m=02&d=20171022&t=2&i=1206544557&r=LYNXMPED9L0TA&w=940" alt="Photo"/>
              </div>
           
            </div>
            <ul class="list-inline">
              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Partager</a></li>
              <li><a href="#" class="link-black text-sm"><i class="fa fa-user margin-r-5"></i> s'inscrire</a>
              </li>
              <li class="pull-right">
                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                  (5)</a></li>
            </ul>

               <form class="form-horizontal">
              <div class="form-group margin-bottom-none">
                <div class="col-sm-9">
                  <input class="form-control input-sm" placeholder="Response"/>
                </div>
                <div class="col-sm-3">
                  <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Send</button>
                </div>
              </div>
            </form>
          </div>
        
          <div class="post clearfix">
            <div class="user-block">
              <img class="img-circle img-bordered-sm" src="assets/bower_components/admin-lte/dist/img/user7-128x128.jpg" alt="User Image"/>
                  <span class="username">
                    <a href="#">Sarah Ross</a> a crée l’événement <b>MEDITATION</b><small className="label pull-right bg-yellow">Amie</small>
                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                  </span>
              <span class="description">Sent you a message - 3 days ago</span>
            </div>
            <p>
            Rejoignez moi demain soir pour passer une heure de zénitude !!! 
            La détente sera au rendez-vous....
            </p>
            <div class="row margin-bottom">
              <div class="col-sm-7">
                <img class="img-responsive" src="http://debuglies.com/wp-content/uploads/2018/04/First-Ever-Neuroimaging-Study-of-People-in-the-Midst-of-Transcendental-Meditation.jpg" alt="Photo"/>
              </div>

              
            </div>

             <ul class="list-inline">
              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Partager</a></li>
              <li><a href="#" class="link-black text-sm"><i class="fa fa-user margin-r-5"></i> s'inscrire</a>
              </li>
              <li class="pull-right">
                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                  (5)</a></li>
            </ul> 
            <form class="form-horizontal">
              <div class="form-group margin-bottom-none">
                <div class="col-sm-9">
                  <input class="form-control input-sm" placeholder="Response"/>
                </div>
                <div class="col-sm-3">
                  <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Send</button>
                </div>
              </div>
            </form>
          </div>
          <div class="post">
            <div class="user-block">
              <img class="img-circle img-bordered-sm" src="assets/bower_components/admin-lte/dist/img/user6-128x128.jpg" alt="User Image"/>
                  <span class="username">
                    <a href="#">Adam Jones</a> a crée l’événement <b>TENNIS</b><small className="label pull-right bg-green">Famille</small>
                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                  </span>
              <span class="description">Posted 5 photos - 5 days ago</span>
            </div>
            <p>
            Venez nombreux ...... On vous invitent à un entraînement gratuit vendredi
            </p>
            <div class="row margin-bottom">
              <div class="col-sm-7">
                <img class="img-responsive" src="http://images.larepubliquedespyrenees.fr/2018/06/07/5b192ec3a43f5e0176512faf/golden/1000x625/de-nombreuses-animations-sont-proposees-pour-la-fete-du-tennis.jpg" alt="Photo"/>
              </div>
             
            </div>
        
            <ul class="list-inline">
              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Partager</a></li>
              <li><a href="#" class="link-black text-sm"><i class="fa fa-user margin-r-5"></i> s'inscrire</a>
              </li>
              <li class="pull-right">
                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                  (5)</a></li>
            </ul>

             <form class="form-horizontal">
              <div class="form-group margin-bottom-none">
                <div class="col-sm-9">
                  <input class="form-control input-sm" placeholder="Response"/>
                </div>
                <div class="col-sm-3">
                  <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Send</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="tab-pane" id="timeline">
          <ul class="timeline timeline-inverse">
            <li class="time-label">
                  <span class="bg-red">
                    10 Nov. 2018
                  </span>
            </li>
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Amé Gasc</a> t'a envoyé un email</h3>


                <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">voir email</a>
                  <a class="btn btn-danger btn-xs">Suprimmé</a>
                </div>
              </div>
            </li>
            <li>
              <i class="fa fa-user bg-aqua"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> a accepté ta demande d'ajout au communauté
                </h3>
              </div>
            </li>
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                <h3 class="timeline-header"><a href="#">Jay White</a> a commenté ton événement</h3>

    
                <div class="timeline-footer">
                  <a class="btn btn-warning btn-flat btn-xs">Voir le commentaire</a>
                </div>
              </div>
            </li>
            <li class="time-label">
                  <span class="bg-green">
                    09 Nov. 2018
                  </span>
            </li>
            <li>
              <i class="fa fa-camera bg-purple"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                <h3 class="timeline-header"><a href="#">Vous avez</a> crée des événements </h3>

                <div class="timeline-body">
                  <img style={{position: "relatove", width: "136px"}} src="https://img.20mn.fr/9CV2ODW0Tgqb0N6tyRCSew/830x532_salle-escalade-pic-paroi-ile-nantes.jpg" alt="..." class="margin"/>
                  <img style={{position: "relatove", width: "136px"}} src="https://www.scenolia.com/media/catalog/product/2/2/22128.jpg" alt="..." class="margin"/>
                  <img style={{position: "relatove", width: "160px"}} src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXFxcVFRcYGRcYGhcYFRUXGBgaFRcYHSggGholHRcXITEiJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGzAmICYtLy8tLS0tLS01LS8tLS0tLTUwMi0tLS0tLS0tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLf/AABEIAKkBKQMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAQIEBQYAB//EADoQAAIBAgQEBAQEBQQDAQEAAAECEQADBBIhMQVBUWETInGBBjKRobHB0fAUI0JS4RVicvEzgpIkB//EABsBAAIDAQEBAAAAAAAAAAAAAAIDAAEEBQYH/8QAMhEAAgIBAwIEBAUDBQAAAAAAAAECEQMEEiExQQUTUXEiYYHwBpGhweEjMkIUFVKx8f/aAAwDAQACEQMRAD8A8/RJoqWYpbYO0a0RcLcJjKa7qR56UvmOsWQTrXLhJfbSaP8AwLggFSKveBcP8S4FO3OjpJWzPLI7qL6nLwslMwXQVFuWSVIAr0zC4FQmUARVHxLggGZl9aVDUJumBl0mSEVJcnnZwDTRVwRFXQXzbURkitVGaWqkVtjBdal2sDNFzgUJrxmBUFb5SZPw+GtqNTrVdi7hJIUVIe1AmnWioG1Uk0DvXWiJg+Hsx2rX8M4WqCWiazdvEvPlB9qkPiHI8z+w8x/T70jUTUV8UkjbpseTJK4wcn+hf4vE2gDBHtWYxFwMTApVkjRT6k/kIoZwrb7egFYf9xwY+jbOmvBNXn5nUSPdSlt4eaK2FPMk+9KmGjYke5oH4zHtH9TSvw3OucnPt/IJsNQACu1aDg+EUt4bTD6Bt8jcj6darcRhCLhRtCCQfUV0NHro51a6nJ1+gy6KdSdp9GMweKZNTJqXgeKBrnn0HKiLgxl31qtuYTU1tU4yuzNHUZIdS6xfGFLKinc61ZtdVUmaxxwcGadincjc1PKi6SZphr7fJrMN1nekQ5205Vl1x1wLAPvT+H8We3M61TxdXY+Osg+pqMSY0503w4Ems0eMsbmY7dKLjfiKRlAqvKlwg/8AU42XeGMnai3WANRsDxK0LcyNqTAXlukmdKBw5ba4GLZJdg9xJE0Sxa0oOOvBTlFTsDbhZNLlFKNlLBjvoMJNBvEiDQ8fj1U0exeDrND5VK6Bngi1SfI9Zih2lJOutFC6aVEt3fNQLG2KWnbfJaWbNSPA9Kq8RfK7UL/VD1oVgnLlDlBLgoLfBVF3MBInarC9cVLg8gH72rU4HhyhQI1qo+L8KVyso0Ag0MMynKjPm08seJzAC0jaso1+1OvqtjzAb1DwOMuMvykkbQJn6VZ+F4gUuNKNpp89DPGSmvh6kK58Q3CICwOtT+EcSzAhtepNdi8AjDkBypTw4eEVUxPOhk8W3pQcIanzLTvjoRr/AA+1cLFCM1LguDKDDmT++Vdwnh72pOZSCRmGUfTMdR7VPR11ylZ5xJ+53rPl1sYcRdm7TeEzy1PJHb9+hT3uDZXIABkHUyQJPKNj61X4nhbK0BSekA1qxciu8ftWdeKyXY1y/D0H0kzJWuHXmOXIR66VNt8GZYkS3U/KPQDU+8elaJ8UIiNa7CY8rPkU9JAOnQyNu9Jy+KZZ8Lg1afwPT4viktz+fQohw6B5jP4fSlXCjpVhjLi5idB2/SoD45eXKuVKUpPlndhCMVSVDvB60C5aJ2qJi+KrrrUH/VxEVSixlotVwRYwGUf8mAFLdwDIwXRyYIyHMDO21UH+pSdKJa4oyGVaCP3zothVvsbTh3w25PmcI0SFkFvcDaoHxNwx1C3CPMPK/cf0t+RoPA+PXFK3CM24JHlzRtMb1osRxlL5FtkgMpHues1u0uaOGaaOVr9NPU4nCS9vkzDpcIogNJjbZtuyHcGKim7XqFzyj57KErafYk3TUd21ofizSO1GkXGFEzOpERrSjBg8xUJLwpTijUpluEuwmKw0VWXF1q1ZpFRblsVaH4pNdSJ4kUTDY1k+UxR/CFDuYccqLcxqnEX+JcsGJJ96vP8AXbhTKB71nTablRrTMtE2pdUX5kkvhZfYDGLr4n4V3+sKHyqPL1qmOIJ3FEtMOlVUerItTlS5Ndj+JW0tzImovArq3CWJrOXqZavFdjFAsUdjSY5a6TdtGi41j1BIB12FRPCbqKo3AJmaf4h6/jTYxjFUi3q2+x60jUmMtZhqJBoFh4G9SrmNUKJNec5s7G5OPJRDBeHdyiADrHvyH39qgYvAXUvOVYohls0SDlAJDqPfccu9atWB8widp/So2KwbOc24ESGzCDIMoVOm3SnwzO+TFk0kXHj14orsHggrBsRrpKqhYyZ5qR/jtTb73PMw8icp1aPrFSuLpoow4JO7MxgR1HUzUL+GuOk3HIWNttfWfzrFqJSyO3wvmdbRY4YI7Vy36L9yCbtrLNy65B5eZfvofwq3wRBQeFaypyJET3G596o3xOCTzMubaM05eUQDv79KsLHFL94fy7RRdlLjLP8AxXpWNnUph3BBIAFANwjcz6be5rsZbK63GGmmm2nSaoL+IuXmFqyhcnTTYf8AJthprSVCUnSGTnCCuTpFre4sg6E1XHizOcqAk9FH2pmK4LbtES/ivHm0hFPRR/UO5o1iwzbDT8BXUw+Fbo7pujzur/Eccc3jww3Nd+xAvDEXNQAoG8nb2E/jRW4SPCXMzs7yRHlVQDHSSTB51eaW9hM7gkw3ZgDqKl2ipQ3XUDKApC8hOkA7b1sjpMGPlRv3OZPxbWZri517Lp9fYwr8EQnXP/8ARqwwXwzYchT43Mkg5tv9sbd6nnEKzGAq/bb8zWh+FrFzNnbILbA6mC0baRqBpWjNixRhbijLpdXq55VHzHX1Mm/whazEjOqHVQW1jvQn+DFfzo7ZVnNrOx71ccTxDtcbxNCNAIy+XloddutSuBvKOvf7Ef4rNq8EY4NySXsb/DNfknrPLlJtc9X+x3DuGxYNuNBqp13A1H0oWHWDm0kbTrM6RV7w5htpvr6RVZjMMUYjvp0rzuRUlJHtMcrbiyB8S4bMqXh0Cv68j+VZm8leiW7Xi4a4pAgRt33J+x9qwlwakHcEg+2len8Lz+Zi2vqjxPjem8nUeYukv+yvQGaIyTUo2xTGtV0jj77BeAKEbJo5aKZ4mtWWnIVLdMurFFWlK1VlXyBFEyiiqoppSpZNwwCnqk0jNFOtXJqFOzjhZ5UX/S2oTXmG1PXGOdyaGW7sFF11Htw4jcio+I4UTswqUlt7nyyYp+Mwr21BJ370u30bGxmuqiVjcPZTA1p38C/7NMN1xzpf4h+tFyHcWa7DYwmQT+VT7keFnYwBoaiNbVCFC6TJPapuNZGtlf6dzWB1ao3RjLa76g+A4ty0geVjudgBppOpOtaHF3AgJY5VAksdv+6oLZVEXkOVXPD8VbdT5pnSCZkdOlJycvdXBp01xjsb5K/i3yAYVWuMIzRqoBEg7jXtVRh7r3li7eVFG4Hmf6/Kp+tX/EOFvJa3cdZABRdM0axm9zqdqyd/hjqZa2RJIB0MkgkRG/rSf9GsrtS/M1PxV6aG2eN+66E7B37AfLhsObr6yx8zd5Y6Lqeoqfdw+IZvO62tQIAzsAe5IUdNM1U/DrF0XYRTmXUgcvWtk+HLeE7/APkG/QHnNXPRwxtbnYvF4rl1EXsjtr6/r6mb49wBRbBa+WAkgMQC/YBQJqDbdciIqBSukrIkd9d+9bTieHBANxC6LJyjSWiBm5xWKxIyucsa6wNYBOwrbo4x20lycfxeWXfcpOn2+/2Gm3rPTrVpgMaltCTGoqmW25MQaK4XOAQVWQCY1A5nvWuUFLhnJxZJY3uiWFnCeKTcZoWD9qg3eIBrfgwDro3PenYnEEWyFkWizBZjMQOseoqrC1IQvl/QOc0v7etcv1v9i4TBM5e5btAhRkYMwJzZYzAabUf4QtfzXdifIu5aBG0HqP0qrw7DKwyMzb5gTAXnIG+ta+7h1TCvcW1bJKiYBAKxznU0rNJxW19+DZpI7p+Yv8U2/ujJcZu53zZy7NqdoAPygQeQ0o/w5ci4VP8AUv3Gv4TUe/ic5HkUQI8ojbme9JZvhWVgNQdfTbT2mjy492Fw+QjT6ny9THJ6P/00uWGmY70XGjPbzaSN4/OmPqKUr/KIA1P7+teQafMWfS006kg3AWDJdtzqymPbtWJ4th4uE9dfyP3FW1nOjhlPmB0/zTPiDCkrbvKZDSG/2tzU+810PCMtZVH5Ucj8Qae9O5Ls0/2Zn4Aok0tKFr054RsBciheDrUr2plxDyqBxl2BhKJAqOtpprgj9aoOk+4dniuBBob2p3NNVQKhSSCNYoZtkVMtN5aCza1AYyd0CFuaG6EUQmlz1GMtjsJxBk+UwaffxbuPMZqLlE0omqot12GEGug9KVSRRZqF3QZ+NXGADMBI1McjQ7OOIJm6SCfL0ImKzz4knU+mtPs3SdBtMxymIrDKcYnTx4ZzdWW2O4w+cw+ZRsfyq04Dxt7RJXWdNdQCYgjuJ+9Zy0pZgAJ12AnT0Fb3C8KTDWzes/zmaYby+QAg5gCPm3GtLU66rgfkwqaqDprubXBXGZAzDKYBjqCNyNxUTivCPHKuLhQgFTl1lW3H+e9YXBfEt0YpXvZ4YoLiqCRosae5mBXpmCvq6yu3LSPtWeSeN7ka4bcycJr+fnwV/AuFiwCoYsZ3PISTAHqatSuu1Py0RjSZTcnbNOPFHHHbFUiFj8NnRgHKNBGbp10rO8P4AFuJckwCIVh9yfWtPidAY3jTvzrN8L4tcuXDmUE+YwCRERGlaMLnte3oYNZHB5sPMXPYusVggx1ABPSs/wDEPCn0yjMAe01ZcRxhFsTKQZJ5+lRbuJvXBNkF1I0bbXnoaLDvi1ITrHhyJwp3x06/kDxeAU4IJbRy2YMQRqG/qn/FVeK4QluzmdiLpgqnIgn01gTNaHB4xktO10hWSZWRqIBH11qj4rxg33hfkUjLpt3b3p+KWS2u13Zg1ccCgp/5baSr9Sqtuy5whOVhlaBuu8a7VsuB4k4ixluIMgXLM/MRptyqju3VZVBdbYI1gTt19a7ikJatLbZhmJfQxPQmjy/1KVUxOlm8DlO7jXK45v5c+pBu2HsFk2LggggHyE6a9TFCGHDMiru0A+tEsh7hJXM9wSSSZ0G0TUS6WVgXBE69OfKtCvpfJhmrdpfD2Nc+ECEJuABE77c6kLZgetV9i+XHiZSqmMoOsct+cxVza1QTXms2LblaZ9D0mdZMEWuhn8QkE0mEvqSbTiUuaMOYPJh3FWWNw8qWHLeqLC4hReQ6ABhmOvXc1h+LHNNHSajlxuMlZR8TwTWrjITsdD1HI0ACtL8ZWwLk9gduTfl+tZsXRXsdNm83GpHzfX6Z6fO4Lp1XsxwU0Nz3prvQ2eKfRkUWM8Vp7UgJnanzTgRVjbOLdaHkogYGkXSoUEujQRpQYMVMtgEd6iusVLBi+wMpSooqwxODti3bZXzMQS4j5TyE1BIqk7GP0B+HSuhogpjMRyqiWxBaPKl8M09H7USoVuZmGtxJZRAgAdZq0v8AAby20eBlcEqQdJ136bGuSyCQrJpyMzy03rVJxW0tgW3URp5V5df33rFPE7+R1YahJccP1E+GMfYw+GDNIumQXCzEHSPSouL4zi2UuLudVdQDkAEmdCkayOtQPFRyFSYJkIRm1PKOdX2EseCsucvmDhdNSF2A6jnPapKMVyDDJKXHoWnw3YHg+HeVjcc5iY+U6HKI2iK0aoyfL2HsOtZzD/EajEZZXIARIknMdNxpFTsT8QqpiVM66an6Css8c76G/HnxVd9C9sYtpysvL5ht9N6Mb43JjvVZwzGFxmE5WWSOnpTzZLA5SGVmB1nQaTEb0hxp0zXHK9trkFxm4+bRoUCYG5Ecj61BsXLTFWtjLcT5uRYHcHrVxj7LFSVAzAiBMSBpBPKsL8WWrv8A5LdhkZQc53z7RkCan1MU7G1Rm1EXuvr99vQ0t1/HRlEyD8pHQ0fgV/LnssyqynNCn+7kDzI0rP8ACcUgUgi9MS2YgDbkDr/1SrgLln/9Acsg86hgJE6DNG+9HScXH8hHxRmppW+/sWnxNhrT7EeLMnXUgdqg21C21tgbyXIGsd6jY7iYw7KcSZZ9ZVTAA5GNt6prnxHaU57ZdkLnNyET16U7He3bZk1GNObmlV8P79WX3CLqeK1s2w4IIE/096ZxDCi0AcmsnQ6iCD+FAv43ILdwJvDZgf6SdtKtMdftXWHnBETlnmaNyakn2M0cUXicLW5Pj6lDw/HG02ZfSrdntXrDACbxIMH7welSOJ/D9vwg1vR94nes8lu6hBAZSdtN6YnDL8UXTFPHl0z2TVpr36+nzLfh6sma07nMACFmQIGlahW/lj0g1n8BhTdjMIywWbZiZ2qxa+RI13rk66Ud3zPS+DQn5bvp2GYrEeEpLCQazPjIbwCnQtz/ADq0+IXBsiZ+brr32qZf4bYIBFoDSVMco0rmOG5nooTUI+5j/iPjQuXjnuLoBPmAiOQG9E4dwpW/mXjcVY8tsFRPdzEg9FG3MmoPAuGYX/UL9pkS5dtXC6PA2bzCYGrAk7nlW04zaFqw9zcqpaDzMaD6wPeuhHNkhFY4OuTly02DLk87Ku1HknxvxiLhsWPIqxnK7lt8uaZgeu/pWdw3GL6GQ5PZpYH60mJtPmJbViSSepJkmol22adc13diNmKXCiq9jU8M+JUbS6fDP92rL7wMw+9XpGgO4YZlI1DDqpGhFefZxyBUGMw+bY6xP1irrgXHns/yz57bnMysNA7c018p21Ed5rVh1clxLlGDUeH45c4+Gac5o00FGsqSfNtS2Gz2/GSSkxBjMvTOB6aHn2qObrGulCSkrRxcmOcXtaosLYg6Ge1S8G9rNNxTlHJeZ9ap7JPOjoBzqNWZ3Ha7DXbpJMCATt0oRBjlRDaMabUhSoDaGhdN6XLXZaVhVF2IdKb4nY/Wn6Unhr0qFFfduMfmGmbNE6FjvpVynDw8MCLbBQfDBzOTEyc2gkHbkIrO8OxRttMA9Z9ND++tHPEFD+QZZ1ETv/jWlOzouHyst/CQTBmFzHdYPYneg2LpkN80KyqIzFiep7mqt7uZpZm6qOR9f3zqXh5Vw7MVIPpB5iKjXZitriXXC1VfPdksQDaURAIbXOOQjahcQtC5dLhxbAPzf3HpptzprmyCHVzMkGBIAG0E7yTTLvFSQYGZf9wWNT0+00G3uTc1wi8w/GBat5bbSQNRH0jlpV/w3j2e2JEE6AdY/CsHg+IQY8Mame3Oplv4gCwESNeu0HlpptSMmFPk2YNVKPH3Ro+L451B80F9NJlV5Dt61U2cbct5MrHyLk1M+Xo3X1OtRhiWvySGE+SZ33Me2pqg4txJrflW5PI5t57Abx1NA8PHBox6j4uefQm/GHFHuxLqjbqFEdCVJ31H5034Vxd2GtYi74NpyCLTgknWTH9oMc9Kg4W8UEMC5aXLlh5WCgiVb+mI17VWcUvsczZi91soD6R8sBQeo50Sx0lZUsu+TrueqPfwkls6SBl3Ug8onrpEVkePW8AyPluKqEqcgMOgO5UH5t5ishiOI+Kq2WUeUBmcCCSo1APQUG2r3FfKpgQ07yYJjtoD9KqkujCUG+qrkvuBYW6WQJdV01kNmORQdyvXWY3rbY3AuLZuYW+t0ro4Crp/dpPKvMuGcfFo5ESZAZmgAqw5qeQjStFifizEu7DD4dkRyvkChifMqsQT8pY6CRzqpZKXDJ5Fydr6mxwt68wHiqVI0KqDpru3SdNO9X+HcFQTEjlB5aVC4PjrV23mU7SGBILBhyYqSJG3tTExltdFjT+n0k6AdqzTyWjVhwbX6lw1uB5WHXUaT7VFuXD/AFruNzBE+tQOIcRtmBBBJGWQVIkTzqPwzGC7dFvxGBEyCOY3Ancd6wS3NnXiopCYHDi7iCjGQqF4E9QF39ftVlikYMLSMBmtvlBnTw11jqZy6dJp9rS940xKeGVjo0gk89zQ+Iq1xFuWo8a0+dM2x5EacipIPrRwjFMmXJKS49DBfAGAKXMRcZ8zF7ctrrrcL8gZzMQfQ1e//wBB4sEti0IJJBYE7Be/MzlrHfD3GC2Ku2wMqE3CqxDL/NzhX6kBis8wKrPjzHsbttST8hc/+7foij2pt/1RG1+RyKt+yw84B1A+361F4pw0Nbe4gARRP/LvVCcVH69J3irHE8SbwmSfngH00JrS3ZkUaaKphpMc9femXDtpFWF62q2bbgkOcykaajWftp7x6QmXSWMHkO2snfTUfuKBjUaz4M4kEBU6sQR4ZHluodSB/uBkgR6GtLj7dpbg8M+R1zodxvBWROoO43GleWeHoTpA31E69FmT9K1WG4/4v8PbLMSoyszHZoIXVtwTvqNW7U/DmcGYtXpVli/U08jYAn0rvbWhcFW45KuZnMqMqtGYLsNTmEwNeok1bXUS0qqVzN/UxGj9gPUxodIrorIpdDz2TTvG6ZXpcpzH3PTY1K/glZTcw4KwYZDrEz8pmY0O9AQFlJMkqJ56QefajTTFShTBkdKaBRsXcJgnoOWgnWBHr96i5+e3p+lWkUkEMUkrQQ/7NLm7D6VKL2lFaaiK3v8AagqsR3g7jn6bUYChR1JEmzeGxG+3Y0d7ebb9xrUa0ZgaSTz/AFo9grEkwSduYipaESVcocFI79Rrr6UNwVjffp+dSUUmIE7ke2/tRGcCVJ6DQyN+Ub/5qNClJ30H4a1nEQdNz09Y2o2GXCAH5mY6GTpMkGII0AgioSW11ykjrB3I7UcYcsubwjCiRJyk7SZ/q5UEo2SLq6sltgrL+U4hlKNJULJAEaAD33+9UR4XZVrjOzMMzFc2gCjXYe1XmE1Webatmgn/ANm1B9uldirF4IqFLN3D5gzh4BXORtlEz0339qCcUlYzDkbk49EZt1s5WOfOyj/cVOu0btoB0pL+NF9UTNaUIJQEKTm5gkkBR9zA6VrOOcASxDJEMJKWySVMGQDtljl/3WF43w+wdUtXsxEmJn3VxrtQSTcLRswyi8m13f0dFdetsw8lxn12AygSJP5UJOJwHUk6wVg6BgIE9QBNNwnD1uHJbdw5JCqyEyQJhSs60dfhu+2VwmbMzDICc3kALSN1359+lZHv6pHT/priT/YrLl1iSRrOkhd6kYPid626+HdcgMGAJMEgjdZ7fYVr/h7gNrEW7niWzYdCGV4yqQIAVwR5tY13IJ10rm+CjbXxPCuXGkkwygb6ZQN+uvI1XlSbI9TjXDNJa+JnezcVbBW/bklSpHiBCCwVQR5iCT69aiYniF+6StvEKhAlkvWSjCW2khQfmGgneq//AE3E22F22LKsw1ALl0gZfMc5OYa661G46bV0sHZkObSWkSYAPllmYTJOs7b1bwcC46rmor+CQ3xEbTm3dZXyyZsSYOxJJ22+1WXBPicXnl8QltbZHhqw8zdZYCToesaisqnFbBYBranyZM0NnuRprlJ9NTpB05UXh+HwYulnVhbhYJJ30zR05x7UCwxvga9RNR5Tv2PWcLjrdweVpDTBGu1ddx+S3l/pMzpr/wB6VgU+IhZd1wyTbnMoMRtq0f3mfp1q3ufEIC7XCYMhQAQGEQQYgE/hSJ6d/wCPI/FrI8eYqKn4twsH+Is7nzOBrB2ziO2jDmNe9Yb4m4j411XiItqpHcFidfevQWxFxwqi2QQDq5VNJ1zAS3/1FY7iWAs5tfEn5suUKonYF9IHfWix4J1bReXV4m9sXd9aM3uQAJOwqe3Db2XO9t1XqVIHQb99PetFgrJUaWipyF9EXyrIElpkbxJnflQ1COc10C6ltZRSW1kaQPLm1gx0FaPJ46mR6nnpwUWHfKJ8GRrDtJyxMkDYxMx1g8qTNazGEdhHMmZP/EdavOI21WAMNb1hhkciNJyuACJBjnI96DheJKsrlUMw3acoX5iQAYJidRrIFU4JPlhrK2rSKq3hCc2hB3GkiO7DQAUXCWApz23BK5fnEasIZQomSJiefLXa1PGUdRM5zKjecrl5VXmRox3J9KfibyBlt2EgMZEQChAiGaNR8x3oljj1sDzZLhoufgv/AMVy1iHIUkPbOYIoceXTMPNPlBA0gHoAblxnVWZgzLmkgA8lOo1gDsOdUvDraMstJHI6zmb+rptrEawNqs8PgBMpdHJoBghVWWkHc5p06a1shDYjlZ5eZLguOF4rNbNoqUC5jsSDAguJ2AkSOrDrVdxW8CRlAEqNRzBJP5ntUvAOVZiJzXDCazlMSASvMwR6HpNVuMtsCudSpywAexgwPWaPHW4x5U6RGLe9Jzp2SugdDrWgSNYikzU4LXZT0/GoWZ9LRidakLbI3B6e9DVomNPTej27z6KG3017+tITOlKzghNE8M86R7ziBM9ft9aPbv8AIxtEfvSjQmViF2kFSViNid4g7cjrp3o1jEMHznUkQZ05RoY0Peh+Ov8Ab7DtTVxPRTOnWPrFRqIFyfRCIG1MsJO4J0EzqI1P6V13iL/JmdgGgNl3UQY/uC9t9KlW8QvMVwxCkwFMdT+fSp5a7FLK31iT04ki2rSBmByy5OU5yGnKRG3v7a1DbFKXLo6hSZyZ4IJOgAgmO9Fa5bAlio9dd6bbNrlEbGIGvp1qtldBfmJq3Fi3MYFMkFZOkHNJ121Mz+dCXG231dJAB11Eg8iZmNPlNSF8E/1AxoQIOv60F8NZ+VTBGscjPXSDVtWVCUV2aY6xjLambVlEmQDl0M8jKSdJETUt+O37YnMAoUoFJkNI0IyiR/bv16UPD4a0uqtBIAI6Qe8/brUfHYgT5LefqJjl/adI33NA4KugyOS58fqEufEZAyjKITM4iJgADLoMw+XXoIqlufEbG4rM6ZDIZczESZGggnUR02qywGDs5fPayamFkNE9NdKJa4fhwxbIJHNgJJJ+1D5Uhyz4Y2nFsyeKxN64rM1xyskC2gYCANySOm1QWwLBho4BPy6tBOmojY616HaAkgARsvKR6TQMRlSc8tGuUdB3PftQS06fVjcfiNPbGH3+RjP4iGjwGBUEGD5c3I6CNOnfetHh/wCHuJN8usQAtu2CfNAPmnnlnQTNdischWRbuHUgBQSRGvLWO9QVxPmOVWI0JJDKe/8AQCx9TSvJjF9TStROa5jVE3B4nD2bwVEOQEwzgqeoA11kxpzkdBU/B3mIE4dSQWAlpGVhBBXQkgdTr71VYW6wbUMBmnyKFLDlIK6fU12Pe/cKhcypJ9SD1A06/WmKNIzybcqte7ZYYvFXtQMqHzaqeRI0gEGNO9VLYQa55YE5iQqwN5zFiTPeoz8LuMRMmf7tQBHMTp6xUqxhntqFVUIBlfISQ0CTJkDaptf/ABCVRXE19Ctu4xLQiyzAcyHMRpuoOu34UDxbM5nBcncsBrm5nrvU9sCiibyQu3zTA5aAaa1yYdJJXORzkHX1JpeyT9DQskK4v39fqRsMFX5bi+bULBIbUZdz1Ub1KHCCVJhM0ksGzyRAKwsQ08tda63athlcW9iCYExGvymQR9KtcTxDMVueBOR0cKWyoQgAAZDI6cuVXs46AvK3JU69yBhOGL8iKxbMF1lgdNoXQTI3o9zFKgZcqoyg+YgDVd1k89fvXeNdliCyZ2JOQHSWJ0O0DTlypmEwayTcWW2DdOcjXn1pkYvolQmcly5OyAL+WSbgPmkZdBMaBlEaelWeCxnh+UM0xJnNOoEgDludeeUbbmc1tCTNsSQqgwJ8o3juZ+1JjALiBSx8ohTGoG5APufrRLFL1Fy1EHxRJwnFLCgechYbNkEG4piFKTpz17jaJqFiuOm4EASFQFV357luvX3oWNwmrFT5SJ8xEj1KwDHtQ1wU8l217zQSWS+C4PDXLJCcRPMR9ftBqRaxQPKq/wADLy5cpj7b01H9T60UJZF/cLyY8MuYFutztHakzfuP8VHW4Y1GnpRv4g9/p/mnWzHsKJDpy36a86KW15HsNqASJ/xUi1H67D6UtHQlwPRievpOn0oqsD6dYptqSdBP396Mqk+lNQiTBqREZvsKME/3T6GlVBP50ZbA5+p60QqU0MVB+yKfp1p7IPtrSqo15fj9KKxTYNranltrqNvSmvhkY+ZQfXWpQX986aU71ClN9mR/4K3EZBHbSkTCqDILehJI7afWpK9zSqoPaq2ovzZLudbtgbR6VHxmEDrlJKjnl0mNtaM1jXyn995oKWrs6uCOkbemtR0yoNp7lIDZ4dlXKt26Oe6k/UrRkwmX5rjk76sPyFHFkzz+tcuHidzrOv5VFBIJ5pPq/wBAN2yBqCwbkQFJ9iR7e9GFgHUjcAEGNY60aO1NC/sVdIW8ro5bAAgACuCc67MOpoaJqTMyfSPSrBtvqx5Hcc+/KmtbB3Fd4cHnrz0pIg86hfToIQOtRLWGUGc7E7asTt7xUwimlRVUHGbXFjXUEQYI3NMKTpFGNMNWRSYwCmm2N41p4pSBtULTYMDpTktAyDGh1/KuWkuWwdxVMvcO6TShdKXwxuBHKutWgDoKoFtCqRIBMfvn0p9i4o0cSOsx+FcFg7U4iqBbGvl3BaKiXVGwYb81NS1t9Ka6fs1VBRlRFugDU6jpED8aTLb6n6j9Kk2rS5oceXqOVd/Dp/fQtDFNfMzllutSrbH26RUNPlajjc+n5UqLOhNFjbEcu/0033qQjacp9PtUZPlHoadZ+YU5GN82Sc3PTtr/AIriT1rl29v0pDt7UQkUHvzp+UULl70UcqtFMIKX3pv6U2z+dQCgsd6d4Q0p1ukXY1LFts4qKSBzpV3FMv7VCLngebZOv4U3J60mD3HvR6lkbp0CCAU6ByM0K9TsL8p9RV9i64sKyihxT6YagCYkTSEUoprVBgs0w11JVhJHEU2lNMaoWhxFcTSNSneoWOdINKKY/P2pUqA9hymnxSLSHeqKFJFcW7016Q1KJQ8k9a71pBXHY1RKOy6UyaVaZVUEj//Z" alt="..." class="margin"/>
                  <img style={{position: "relatove", width: "180px"}} src="http://www.slate.fr/sites/default/files/styles/1060x523/public/cine_1.jpg" alt="..." class="margin"/>
                </div>
              </div>
            </li>
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <div class="tab-pane" id="settings">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Par nom</label>

              <div class="col-sm-10">
                <input type="email" class="form-control" id="inputName" placeholder="Nom"/>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Par email</label>

              <div class="col-sm-10">
                <input type="email" class="form-control" id="inputEmail" placeholder="Email"/>
              </div>
            </div>
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Par événement</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputName" placeholder="événement"/>
              </div>
            </div>

             <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Par communauté</label>

              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputName" placeholder="communauté"/>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Lancer la recherche</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

    
  </div>

 
    </section>
            </div>
            </div>
        );
    }
}

export default Dashboard;