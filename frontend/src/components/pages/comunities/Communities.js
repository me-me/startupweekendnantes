
import React from "react";
import Header from '../../common/Header';
import Sidebar from '../../common/SideBar';
import Footer from '../../common/Footer';


class Communities extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <div>
            <Header />
                <div>
                    <div class="content-wrapper">
                    <section class="content-header">
<h1>
    Gestion des communautés
    <small>admin</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">Gestion des communautés</li>
</ol>
</section>

<section class="content">
<div class="row">


    <div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border">
        <h3 class="box-title">Creer une communauté</h3>
        </div>
        <form role="form">
        <div class="box-body">

        <div class="form-group">
            <label for="exampleInputPassword1">Nom</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nom"/>
            </div>

                 <div class="form-group">
                    <label for="exampleInputPassword1">Description </label>
                    <textarea rows="5" cols="23" type="text" class="form-control" id="exampleInputPassword1" placeholder="Description"/>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Type</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="type"/>
            </div>

            <div class="form-group">
             <label for="exampleInputPassword1">Modalite</label>
            </div>

            <div class="form-group">
            <input type="radio" class="form-control" id="exampleInputPassword1"/> &nbsp;
             <label for="exampleInputPassword1">libre acces</label>
            </div>
            <div class="form-group">
            <input type="radio" class="form-control" id="exampleInputPassword1" /> &nbsp; 
             <label for="exampleInputPassword1">validation du moderateur</label>
            </div>
            

 <div class="form-group">
             <label for="exampleInputPassword1">Localisation</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Localisation"/>
            </div>


         
          
        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>


<div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border">
        <h3 class="box-title">Ajouter des invités </h3>
        </div>
      
        <form role="form">
        <div class="box-body">
        <div className="input-group">
                  <input type="text" name="q" className="form-control" placeholder="Search par e-mail ou nom..."/>
                  <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                      </span>
                </div>
                <div class="box-footer">
            <button type="submit" class="btn btn-primary">inviter</button>
        </div>
</div>              
        </form>

        <form role="form">
        <div class="box-body">
            <div class="form-group">
            <tr>
                <td>     <h3 class="box-title">Ajouter via un fichier</h3></td>
                &nbsp;&nbsp;&nbsp; <td>          <input type="file" id="exampleInputFile"/>
  </td>
            </tr>
        <div class="box-footer">
            <button type="submit" class="btn  btn-primary">inviter</button>
        </div>
        </div>
</div>              
        </form>
    </div>

    

</div>

<div class="col-md-12" style={{marginBottom: "40px"}}>
    <div class="box box-info">


<div class="box">
    <div class="box-header">
      <h3 class="box-title">Liste des invités</h3>
    </div>
     <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nom</th>
          <th>Prenom</th>
          <th>Address e-mail</th>
          <th>Role</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
      
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        <tr>
          <td>John</td>
          <td>Piere</td>
          <td>j.piere@habee.com</td>
          <td>user</td>
          <td>Active</td>
          <td>
          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
              
              <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
            </div>
          </td>
        </tr>
        </tbody>
    
      </table>
    </div>
   </div>

    </div>
    </div>






</div>
</section>
                    </div>
                </div>
            <Sidebar/>
            <Footer />
        </div>
        );
    }
}

export default Communities;