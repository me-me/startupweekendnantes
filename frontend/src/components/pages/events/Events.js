
import React from "react";
import Header from '../../common/Header';
import Sidebar from '../../common/SideBar';
import Footer from '../../common/Footer';


class Event extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <div>
                <Header />

                 <div>
                            <div class="content-wrapper">
                            <section class="content-header">
        <h1>
            Gestion des evenements
            <small>admin</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gestions de sevenements</li>
        </ol>
        </section>

        <section class="content">
        <div class="row">

        
            <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                <h3 class="box-title">Ajouter un evenement</h3>
                </div>
                <form role="form">
                <div class="box-body">

                <div class="form-group">
                    <label for="exampleInputPassword1">Titre de l'evenement</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nom"/>
                    </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Date et heure de debut</label>
                    <input type="datetime-local" class="form-control" id="exampleInputPassword1" placeholder="Nom"/>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputPassword1">Date et heure de la fin</label>
                    <input type="datetime-local" class="form-control" id="exampleInputPassword1" placeholder="Nom"/>
                    </div>


                <div class="form-group">
                    <label for="exampleInputPassword1">Lieu</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="lieu"/>
                    </div>

                     <div class="form-group">
                    <label for="exampleInputPassword1">Image</label>
                    <input type="file" class="form-control" id="exampleInputPassword1" placeholder="lieu"/>
                    </div>

                     <div class="form-group">
                    <label for="exampleInputPassword1">Nombre maximum de participants </label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre maximum de participants"/>
                    </div>

                     <div class="form-group">
                    <label for="exampleInputPassword1">Nombre minimum de participants </label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre minimum de participants"/>
                    </div>

                    

                    <div class="form-group">
                    <label for="exampleInputPassword1">Description </label>
                    <textarea rows="5" cols="23" type="text" class="form-control" id="exampleInputPassword1" placeholder="Description"/>
                    </div>

                </div>
    
                <div class="text-center box-footer">
                    <button type="submit" class="btn btn-primary">Creer</button>
                </div>
                </form>
            </div>
    </div>


    <div class="col-md-6">
            <div class="box box-info">
            <div class="box-header with-border">
        <h3 class="box-title">Ajouter des invités ou communautés</h3>
        </div>
      
        <form role="form">
        <div class="box-body">
        <div className="input-group">
                  <input type="text" name="q" className="form-control" placeholder="Search par e-mail ou nom..."/>
                  <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                      </span>
                </div>
                <div class="box-footer">
            <button type="submit" class="btn btn-primary">inviter</button>
        </div>
</div>              
        </form>
            </div>

            
    </div>

<div class="col-md-12" style={{marginBottom: "40px"}}>
            <div class="box box-info">


   <div class="box">
            <div class="box-header">
              <h3 class="box-title">Liste des evenements</h3>
            </div>
             <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Titre</th>
                  <th>Date et l'heure de debut</th>
                  <th>Date et heure de fin</th>
                  <th>Nombre de participants</th>
                  <th>Lieu</th>
                  <th>Image</th>
                  <th>Description</th>
                </tr>
                </thead>
                <tbody>
              
                <tr>
                  <td>Tennis</td>
                  <td>10/11/2018 10:00 </td>
                  <td>10/11/2018 11:45 </td>
                  <td>8</td>
                  <td>Paris</td>
                  <td>Venez nombreux ...... On vous invitent à un entraînement gratuit vendredi.</td>
                  <td>
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                    </div>
                  </td>
                </tr>
                <tr>
                <td>Meditation</td>
                  <td>13/11/2018 15:00</td>
                  <td>13/11/2018 18:00</td>
                  <td>18</td>
                  <td>Plais des congres 75009</td>
                  <td>Rejoignez moi demain soir pour passer une heure de zénitude !!! La détente sera au rendez-vous....</td>
                  <td>
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                    </div>
                  </td>
                </tr>
                <tr>
                <td>Soccer</td>
                  <td>15/11/2018  14:00</td>
                  <td>15/11/2018  16:00</td>
                  <td>8</td>
                  <td>Bercy urban</td>
                  <td>Pensez à ramener votre bonne humeur !!!! Je compte sur vous !!! A samedi.</td>
                  <td>
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-flat"><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                    </div>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
           </div>

            </div>
            </div>






        </div>
        </section>
                            </div>
                        </div>

                <Sidebar/>
                <Footer />
            </div>
        );
    }
}

export default Event;