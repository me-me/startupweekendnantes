
import React from "react";
import {ToastContainer, toast} from 'react-toastify';





class Login extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

      
      return (
            <div className="login-box">
            <ToastContainer/>
            <div className="login-logo">
              <a href="../../index2.html"><b>Admin</b>Community</a>
            </div>
             <div className="login-box-body">
              <p className="login-box-msg">Connectez-vous pour commencer votre session</p>
          
              <form encType="multipart/form-data">
              
               <div className="form-group has-feedback">
                  <input  name="email" type="email" className="form-control" placeholder="Email" required/>
                  <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
              
                <div className="form-group has-feedback">
                  <input  name="password" type="password" className="form-control" placeholder="Password" required/>
                  <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
              
                <div className="row">
                  <div className="col-xs-8">
                    <div className="checkbox icheck">
                      <label>
                        <input type="checkbox"/> Remember Me
                      </label>
                    </div>
                  </div>
                   <div className="col-xs-4">
                    <button   className="btn btn-primary btn-block btn-flat"><a style={{textDecoration: "none", color: "blanchedalmond"}} href="/dashbord">Sign In</a></button>
                  </div>
                 </div>
                 
              </form> 
            </div>
           </div>
        );
    }
};


export default Login;