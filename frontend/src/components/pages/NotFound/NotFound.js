
import React from "react";
import Header from '../../common/Header';
import Sidebar from '../../common/SideBar';
import Footer from '../../common/Footer';


class NotFound extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <div>
                <Header />
                <div>
                        <div class="content-wrapper">
                            <h1>The page you are looking for does not exist!</h1>    
                        </div>
                     </div>
                <Sidebar/>
                <Footer />
            </div>
        );
    }
}

export default constructor;