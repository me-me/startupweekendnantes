import  React, { Component } from "react";
import { Redirect, Link, BrowserRouter, Route, Switch } from "react-router-dom";
import { router, route, Browserhistory } from "react-router";
import Home from "./components/pages/Dashboard/Home";
import decode from 'jwt-decode';
import LandingPage from './components/pages/landingPage/LandingPage';
import Login from './components/pages/Login/Login';
import Event from './components/pages/events/Events';
import Communities from './components/pages/comunities/Communities';

require('dotenv').config();

class App extends Component {


    render() {
        

        return (
            <div className="row">
            <BrowserRouter>
                <Switch>
                    <Route exact path="/dashbord" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/events" component={Event} />
                    <Route exact path="/communities" component={Communities} />
                </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;